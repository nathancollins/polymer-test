FROM selenium/standalone-chrome

USER root
RUN apt-get update

# Install Git
RUN apt-get install -y git

# Install ssh-agent
RUN apt-get install -y openssh-client

# Install Firefox
RUN apt-get install -y firefox

# Install xvfb
RUN apt-get install -y xvfb

# Install Node
RUN apt-get install -y curl 
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN apt-get install -y nodejs

# Install yarn
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update
RUN apt-get install -y yarn

# Add json - merges json files
RUN yarn global add json-merge-patch-cli

# Install bower
RUN yarn global add bower

# Install Polymer CLI
RUN yarn global add polymer-cli

# Install firebase CLI
RUN yarn global add firebase-tools

RUN yarn global add webdriverio \
    wdio-mocha-framework \
    wdio-selenium-standalone-service \
    wdio-spec-reporter \
    wdio-webcomponents

RUN firefox -v

RUN google-chrome --version

RUN polymer --version

RUN yarn --version

RUN bower --version

ADD startup.sh /

RUN chmod +x /startup.sh

ENTRYPOINT ["/startup.sh"]
